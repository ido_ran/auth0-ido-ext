const request = require('request');
const debug = require('debug')('auth0:IpSetFetcher');

class UrlFetcher {

    constructor(sources, callback) {
        this.sources = sources;
        this.callback = callback;

        this.ipset = [];
        this.sourcesModified = 0;
        this.sourcesWithUpdatedEtags = [];
        this.sourcesToLoad = 0;
        this.sourcesToCheck = 0;
    }

    update() {

        this.sourcesToCheck = this.sources.length;

        for (let index = 0; index < this.sources.length; index++) {
            this._checkForUpdate(this.sources[index]);
        }

    }

    _checkForUpdate(source) {

        const that = this;
        const {url, etag} = source;
        debug('requesting for update ' + url);

        const options = {
            url,
            method: 'HEAD',
            headers: {
                'If-None-Match': etag,
            },
        };

        request(options, function (error, response) {
            debug('got response for ' + url);
            if (error) {
                debug('fail to load url ' + url, error);
                return that.callback(error);
            }

            // If the conent was not modified
            if (response.statusCode === 304) {
                // Nothing to do, just make sure not to increment sourcesModified.
            } else if (response.statusCode === 200) {
                that.sourcesModified++;
            }

            that.sourcesToCheck--;
            if (that.sourcesToCheck === 0) {

                if (that.sourcesModified === 0) {
                    // Nothing was modified since last load.
                    that.callback(null, {wasUpdated: false});

                } else {
                    // We need to load the sources, something was modified.
                    that._loadSources();
                }
            }

        });
    }

    _loadIpSetFromUrl(source) {

        const that = this;
        const {url} = source;
        debug('requesting ' + url);

        request(url, function (error, response, body) {
            debug('got response for ' + url);
            if (error) {
                debug('fail to load url ' + url, error);
                return this.callback(error);
            }

            if (response.statusCode !== 200) {

                return that.callback({
                    error: 'non 200 http status code',
                    file: url,
                    statusCode: response.statusCode,
                });
            }

            const etag = response.headers['etag'];
            that.sourcesWithUpdatedEtags.push({
                url,
                etag: etag,
            });

            const lines = body.split(/\n/);
            for (let i = 0; i < lines.length; i++) {
                const line = lines[i];
                if (line.length < 4 || line.indexOf('#') !== -1) continue; //comment
                that.ipset.push({
                    ipOrCidr: lines[i],
                    file: url,
                    etag: etag
                });
            }

            that.sourcesToLoad--;

            if (that.sourcesToLoad === 0) {
                return that.callback(null, {
                    wasUpdated: true,
                    ipset: that.ipset,
                    sourcesWithUpdatedEtags: that.sourcesWithUpdatedEtags
                });
            }
        });
    }

    _loadSources() {

        this.sourcesToLoad = this.sources.length;

        for (let index = 0; index < this.sources.length; index++) {
            this._loadIpSetFromUrl(this.sources[index]);
        }

    }

}

module.exports = UrlFetcher;
