
module.exports = {
    CHILD_READY: 1,
    BLACKLIST_REQUEST: 2,
    BLACKLIST_RESPONSE: 3,
    LOAD_SOURCES: 4,
    CHILD_ERROR: 5,
    END_CHILD: 6,
};