const events = require('events');
const {fork} = require('child_process');
const debug = require('debug')('auth0:child-manager');
const types = require('./message-types');

/**
 * URL of the source IpSets.
 */
const SOURCE_URLS = [
    'https://raw.githubusercontent.com/firehol/blocklist-ipsets/master/firehol_level1.netset',
    'https://raw.githubusercontent.com/firehol/blocklist-ipsets/master/firehol_level2.netset',
];

/**
 * List of source.
 * Each obect has url and last etag.
 */
let sources;

const eventEmitter = new events.EventEmitter();

/**
 * The main child process which handle requests.
 */
let mainChildProcess;

/**
 * The next child process which might replace the main or might exit.
 */
let nextChildProcess;

/**
 * Counter use to create unique requestId to match requests to async responses.
 */
let requestIndex = 0;

/**
 * Map from requestId (see requestIndex) to callback function that should be called to handle the response.
 * @type {{}}
 */
let requestToCallbackMap = {};

/**
 * Running debug port, used to be able to debug child processes in an IDE.
 * @type {number}
 */
let debugBrkPort = 5858;

module.exports.startChild = startChild;

function startChild() {

    sources = [];
    SOURCE_URLS.forEach(function(sourceUrl) {
        sources.push({
            url: sourceUrl,
            etag: ''
        });
    });

    const forked = forkChild();

    mainChildProcess = forked;

    forked.on('message', handleMainChildMessage);

    mainChildProcess.send({
        type: types.LOAD_SOURCES,
        sources: sources
    })
}

module.exports.update = function () {

    // TODO: check if we have etag, if not ignore the update request.

    if (nextChildProcess) {
        // The update process is running, ignore this request to udpate again.
        return;
    }

    debug('start update process...');

    const forked = forkChild();

    nextChildProcess = forked;

    forked.on('message', handleNextChildMessage);

    nextChildProcess.send({
        type: types.LOAD_SOURCES,
        sources: sources
    });
};

function forkChild() {

    const forked = fork('child.js',
        [],
        {
            // this override debug arguments to allow debugging of child processes in an IDE.
            // execArgv: ['--debug-brk=' + debugBrkPort++],
        });

    return forked;
}

function handleNextChildMessage(msg) {
    debug('Message from next child', msg);


    switch (msg.type) {

        case types.CHILD_READY: {

            if (!msg.wasUpdated) {
                // There was no update, end the child process.
                nextChildProcess.disconnect();
                nextChildProcess = undefined;

                return;
            }

            sources = msg.sources;

            const previousChildProcess = mainChildProcess;
            mainChildProcess = nextChildProcess;
            nextChildProcess = undefined;

            // Remove previous handlers
            mainChildProcess.removeListener('message', handleNextChildMessage);

            // and write the new child process to handle messages
            mainChildProcess.on('message', handleMainChildMessage);

            // Remove exit event handler because to prevent child-manager
            // from starting a new child in its place.
            previousChildProcess.removeListener('exit', handleMainChildExit);

            // release previous child
            previousChildProcess.send({
                type: types.END_CHILD
            });
            break;
        }

        case types.CHILD_ERROR: {
            debug('next child had error', msg.error);
            nextChildProcess.exit(-1);
            nextChildProcess = undefined;
            break;
        }

        default: {
            debug('unknown message received at parent process', msg);
        }

    }
}

function handleMainChildExit(code, signal) {
    // Restart a child process.
    // Reset etags to get the child to reload all data.
    debug('child process exit, restart a new one');

    mainChildProcess = undefined;
    startChild();
}

function handleMainChildMessage(msg) {
    debug('Message from main child', msg);

    switch (msg.type) {

        case types.CHILD_READY: {
            mainChildProcess.on('exit', handleMainChildExit);

            sources = msg.sources;
            eventEmitter.emit('childReady');
            break;
        }

        case types.CHILD_ERROR: {
            debug('child had error', msg.error);
            break;
        }

        case types.BLACKLIST_RESPONSE: {
            handleBlacklistResponse(msg);
            break;
        }

        default: {
            debug('unknown message received at parent process', msg);
        }
    }
}

function handleBlacklistResponse(msg) {

    const callback = requestToCallbackMap[msg.requestId];
    if (callback) {
        // Remove the mapping between request ID and callback
        delete requestToCallbackMap[msg.requestId];

        // If an error occurred error will have a value,
        // if finished successfully error will be undefined.
        callback(msg.error, msg.result);
    }

}

module.exports.blacklistRequest = function (ip, callback) {
    const requestId = requestIndex++;
    requestToCallbackMap[requestId] = callback;
    mainChildProcess.send({
        type: types.BLACKLIST_REQUEST,
        ip,
        requestId,
    });
};

module.exports.onChildReady = function (handler) {
    eventEmitter.on('childReady', handler);
};