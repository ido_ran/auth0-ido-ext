const app = require('./app');
const debug = require('debug')('auth0:server');
const http = require('http');
const childMgr = require('./child-manager');

console.log('Auth0 IP Blacklist Service 1.0\n');

childMgr.onChildReady(onChildProcessReady);
childMgr.startChild();

// This is the interval at which updates will be polled.
const updateInterval = getUpdateInterval();
setInterval(function() {
    childMgr.update();
}, updateInterval);

/**
 * Get port from environment and store in Express.
 */
const port = (process.env.PORT || '3000');
app.set('port', port);

/**
 * Create HTTP server.
 */

var server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */

server.on('error', onError);
server.on('listening', onListening);
server.on('close', onClose);

function onChildProcessReady() {
    server.listen(port);
}

function onClose() {
    debug('server is closed');
}

function onListening() {
    var addr = server.address();
    console.log('Listening on port ' + addr.port + ' pid ' + process.pid);
}

function onError(error) {
    if (error.syscall !== 'listen') {
        throw error;
    }

    // handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES':
            console.error('Port requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error('Port is already in use, pid ' + process.pid);
            process.exit(1);
            break;
        default:
            throw error;
    }
}

function getUpdateInterval() {
    let interval = 0;
    if (process.env.UPDATE_INTERVAL) {
        interval = parseInt(process.env.UPDATE_INTERVAL);
    }

    if (!(interval > 0)) {
        interval = 1000 * 60 * 10; // default interval of 10 minutes.
    }

    return interval;
}