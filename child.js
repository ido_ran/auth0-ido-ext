const debug = require('debug')('auth0:child');
const IPRouter = require("ip-router");
const types = require('./message-types');
const IpSetFetcher = require('./IpSetsFetcher');

/**
 * The main data structure which is used to match ip to
 * the list of ip/cidr sets.
 * @type {IPRouter}
 */
const router = new IPRouter();

/**
 * Instance of IpSetFetcher used to fetch the data source files.
 */
let ipSetFetcher;

/**
 * Indicate if this child is ready to handle requests.
 * The parent process can send requests to this child even before it is ready
 * to handle them. If it is not ready yet requests are queued in pendingBlacklistRequests.
 * @type {boolean}
 */
let isReady = false;

/**
 * List of pending blacklist requests sent while the process was not ready to handle them yet.
 * @type {Array}
 */
let pendingBlacklistRequests = [];

process.on('message', (msg) => {
    debug('Message from parent:', msg);

    switch (msg.type) {

        case types.LOAD_SOURCES: {
            loadSources(msg.sources);
            break;
        }

        case types.BLACKLIST_REQUEST: {
            handleBlacklistRequest(msg);
            break;
        }

        case types.END_CHILD: {
            // if we got this message it means this is the last
            // message and we should disconnect and exit.
            debug('child got END_CHILD, disconnecting from parent...');
            process.disconnect(function() {
                debug('child over and out');
                process.exit();
            });
            break;
        }

        default: {
            debug('unknown message received at child process', msg);
        }
    }

});

function processPendingBlacklistRequestsQueue() {
    pendingBlacklistRequests.forEach(function({ ip, requestId }) {
        processBlacklistRequest(ip, requestId);
    });
    pendingBlacklistRequests = [];
}

let processBlacklistRequest = function (ip, requestId) {
    try {
        const ipItem = router.route(ip);
        process.send({
            type: types.BLACKLIST_RESPONSE,
            requestId: requestId,
            result: {
                isBlacklist: (!!ipItem),
                ipItem: ipItem
            }
        });
    } catch (exception) {
        process.send({
            type: types.BLACKLIST_RESPONSE,
            requestId: requestId,
            error: exception
        });
    }
};

function handleBlacklistRequest(msg) {

    const { ip, requestId } = msg;

    if (!isReady) {
        pendingBlacklistRequests.push({
            ip,
            requestId
        });

        return;
    }

    processBlacklistRequest(ip, requestId);
}

function onLoadSourcesDone(error, result) {

    if (error) {
        process.send({
            type: types.CHILD_ERROR,
            error
        });
        return;
    }

    if (result.wasUpdated) {

        result.ipset.forEach(function(ipItem) {
            router.insert(ipItem.ipOrCidr, ipItem);
        });

        isReady = true;
        processPendingBlacklistRequestsQueue();

        process.send({
            type: types.CHILD_READY,
            sources: result.sourcesWithUpdatedEtags,
            wasUpdated: true
        });

    } else {

        process.send({
            type: types.CHILD_READY,
            wasUpdated: false
        });

    }

}

function loadSources(sources) {

    debug('loadSources', sources);

    ipSetFetcher = new IpSetFetcher(sources, onLoadSourcesDone);
    ipSetFetcher.update();

}
