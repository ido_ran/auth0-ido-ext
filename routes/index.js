const express = require('express');
const ipaddr = require('ipaddr.js');
const router = express.Router();
const childMgr = require('../child-manager');

/* GET home page. */
router.get('/', function (req, res, next) {
    res.set('Content-Type', 'text/plain')
    .send(
        'welcome to Blacklist\n' +
        '\n' +
        '/ - About this service\n' +
        '/blacklist/:ip - Check if ip is blacklisted.\n' +
        '\t\tResults:\n' +
        '\t\t404 - The IP is not blacklisted\n' +
        '\t\t200 - The IP is blacklisted, details about the blacklist source returned in plain text');
});

router.get('/blacklist/:ip', function (req, res, next) {
    const ip = req.params.ip;

    if (!ipaddr.isValid(ip)) {
        return res.status(400).end('ip is not valid');
    }

    childMgr.blacklistRequest(ip, function (err, result) {
        if (err) {
            res.status(500).send('Fail to handle your request');
            return;
        }

        if (result.isBlacklist) {
            res.send(result.ipItem).end();
        } else {
            res.status(404).end('ip is not blacklisted');
        }
    });
});

module.exports = router;
